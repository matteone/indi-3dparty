/*
 DSUSB Driver for GPhoto

 Copyright (C) 2017 Jasem Mutlaq (mutlaqja@ikarustech.com)

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/

#include "dsusbdriver.h"

#include <indilogger.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>

#define SHUTTER_ON     28
#define FOCUS_ON       29

DSUSBDriver::DSUSBDriver(const char *device)
{
    wiringPiSetup();
    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG,"Setting GPIO pin directions");
    pinMode(SHUTTER_ON, OUTPUT);
    pinMode(FOCUS_ON, OUTPUT);
    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG,"GPIO ready");
}

bool DSUSBDriver::readState()
{
    return(true);
}

bool DSUSBDriver::openShutter()
{
    uint8_t command = 0;

    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "DSUSB Opening Shutter ...");

    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "Asserting Focus ...");
    digitalWrite(FOCUS_ON, HIGH);

    // Wait 100 ms
    usleep(100000);

    // Assert Shutter
    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "Asserting Shutter ...");
    digitalWrite(SHUTTER_ON, HIGH);

    return true;
}

bool DSUSBDriver::closeShutter()
{
    uint8_t command = 0;

    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "DSUSB Closing Shutter ...");

    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "Deasserting Shutter ...");
    digitalWrite(SHUTTER_ON, LOW);

    // Wait 100 ms
    usleep(100000);

    // Deassert Focus
    DEBUGDEVICE(device, INDI::Logger::DBG_DEBUG, "Deasserting Focus ...");
    digitalWrite(FOCUS_ON, LOW);

    return true;
}
